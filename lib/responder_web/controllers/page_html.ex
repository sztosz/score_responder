defmodule ResponderWeb.PageHTML do
  use ResponderWeb, :html

  embed_templates "page_html/*"
end
