defmodule ResponderWeb.ScoreResponder do
  use ResponderWeb, :controller

  @timeout Application.compile_env!(:responder, :time_in_min_to_wait_before_sending_response)
  @webhook_url Application.compile_env!(:responder, :webhook_url)

  def score(conn, %{"ulids" => ulids}) do
    Enum.map(
      ulids,
      fn ulid -> Task.start_link(fn -> send_response_after_long_time(ulid) end) end
    )

    json(conn, %{response: :ok})
  end

  defp send_response_after_long_time(ulid) do
    Process.sleep(@timeout)

    body =
      ulid
      |> Map.put(:score, Enum.random(1..1_000))
      |> Jason.encode!()

    headers = [{"Content-Type", "application/json"}]

    :post
    |> Finch.build(@webhook_url, headers, body)
    |> Finch.request(Responder.Finch)
  end
end
