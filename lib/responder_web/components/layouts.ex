defmodule ResponderWeb.Layouts do
  use ResponderWeb, :html

  embed_templates "layouts/*"
end
